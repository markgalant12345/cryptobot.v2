﻿namespace CryptoBot.API.Data
{
    public class History
    {
        public int Id { get; set; }
        public int? OpenId { get; set; }
        public int? CloseId { get; set; }

        public virtual Order Open { get; set; }
        public virtual Order Close { get; set; }
    }
}
