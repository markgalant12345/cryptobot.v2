﻿using System;

namespace CryptoBot.API.Data
{
    public class Log
    {
        public int Id { get; set; }

        public byte Level { get; set; }
        public DateTime Time { get; set; }

        public int EventId { get; set; }

        public string Message { get; set; }
    }
}