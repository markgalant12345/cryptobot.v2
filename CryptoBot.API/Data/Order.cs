﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CryptoBot.API.Data
{
    public class Order
    {
        public int Id { get; set; }

        public string SignalID { get; set; }

        public byte SignalType { get; set; }

        public DateTime Time { get; set; }

        public decimal Price { get; set; }
     
        public decimal MarginLevel { get; set; }
     
        public decimal Quantity { get; set; }

        public decimal Total { get; set; }

        public decimal Balance { get; set; }
    }
}