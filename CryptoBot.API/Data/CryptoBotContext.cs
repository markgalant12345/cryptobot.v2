﻿using Microsoft.EntityFrameworkCore;

namespace CryptoBot.API.Data
{
    public class CryptoBotContext : DbContext
    {
        public CryptoBotContext(DbContextOptions<CryptoBotContext> options) : base(options)
        {
        }

        public DbSet<History> History { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().Property(it => it.Quantity).HasColumnType("decimal(18,8)");
            modelBuilder.Entity<Order>().Property(it => it.Total).HasColumnType("decimal(18,8)");
            modelBuilder.Entity<Order>().Property(it => it.Balance).HasColumnType("decimal(18,8)");
            modelBuilder.Entity<Order>().Property(it => it.Price).HasColumnType("decimal(18,8)");
            modelBuilder.Entity<Order>().Property(it => it.MarginLevel).HasColumnType("decimal(4,2)");
        }
    }
}
