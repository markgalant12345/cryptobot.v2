﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CryptoBot.API.Migrations
{
    public partial class MarginLevelAsDecimalMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "MarginLevel",
                table: "Orders",
                type: "decimal(4,2)",
                nullable: false,
                oldClrType: typeof(byte));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "MarginLevel",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(4,2)");
        }
    }
}
