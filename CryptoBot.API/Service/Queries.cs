﻿using MediatR;
using System;
using System.Collections.Generic;

namespace CryptoBot.API.Service
{
    public class OrdersQuery : IRequest<OrdersResponse>
    {
    }

    public class OrdersResponse : List<OrderResponse>
    {
        public OrdersResponse(IEnumerable<OrderResponse> data) : base(data)
        { }
    }

    public class LastOrderQuery : IRequest<OrderResponse>
    {
    }


    public class OrderResponse
    {
        public int Id { get; set; }
        
        public string SignalID { get; set; }

        public SignalType SignalType { get; set; }

        public DateTime Time { get; set; }

        public decimal MarginLevel { get; set; }
      
        public decimal Quantity { get; set; }

        public decimal Total { get; set; }

        public decimal Balance { get; set; }

        public decimal Price { get; set; }
    }

    public class LogsQuery : IRequest<LogsResponse>
    {
        public int Size { get; set; } = 100;
    }

    public class LogsResponse : List<LogResponse>
    {
        public LogsResponse(IEnumerable<LogResponse> data) : base(data)
        { }
    }


    public class LogResponse
    {
        public int Id { get; set; }

        public byte Level { get; set; }

        public int EventId { get; set; }

        public string Message { get; set; }

        public DateTime Time { get; set; }
    }
}
