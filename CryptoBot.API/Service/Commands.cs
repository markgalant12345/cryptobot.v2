﻿using MediatR;
using System;

namespace CryptoBot.API.Service
{
    public class CreateOrderCommand : IRequest<int>
    {
        public string SignalID { get; set; }

        public byte SignalType { get; set; }

        public DateTime Time { get; set; }

        public decimal MarginLevel { get; set; }

        public decimal Quantity { get; set; }

        public decimal Total { get; set; }

        public decimal Balance { get; set; }

        public decimal Price { get; set; }
    }

    public class LogCommand : IRequest<int>
    {
        public byte Level { get; set; }
        public int EventId { get; set; }
        public string Message { get; set; }
        public DateTime Time { get; set; }
    }

    public class TelegramCommand : IRequest<int>
    {
        public TelegramCommand(string message)
        {
            Message = message;
        }

        public string Message { get; set; }
    }
}
