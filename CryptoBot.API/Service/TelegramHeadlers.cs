﻿using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace CryptoBot.API.Service
{
    public class TelegramHeadlers :
        IRequestHandler<TelegramCommand, int>
    {
        private readonly IConfiguration _configuration;
        private readonly ITelegramBotClient _client;

        public TelegramHeadlers(IConfiguration configuration, ITelegramBotClient client)
        {
            _configuration = configuration;
            _client = client;
        }

        public async Task<int> Handle(TelegramCommand command, CancellationToken cancellationToken)
        {

            var chats = _configuration.GetValue<string>("Telegram:Chats");

            foreach (var chatId in chats.Split(","))
            {
                await _client.SendTextMessageAsync(chatId, command.Message, ParseMode.Html, cancellationToken: cancellationToken);
            }

            return 1;
        }
    }
}
