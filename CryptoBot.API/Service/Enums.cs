﻿
namespace CryptoBot.API.Service
{
    public enum SignalType
    {
        Long = 1,
        Short = 2,
        Close = 3,
    }
}
