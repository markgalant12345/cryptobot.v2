﻿using CryptoBot.API.Data;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace CryptoBot.API.Service
{
    public class CommandHeadlers : 
        IRequestHandler<CreateOrderCommand, int>,
        IRequestHandler<LogCommand, int>
    {
        private readonly CryptoBotContext _context;

        public CommandHeadlers(CryptoBotContext context)
        {
            _context = context;
        }

        public Task<int> Handle(CreateOrderCommand command, CancellationToken cancellationToken)
        {
            _context.Add(new Order()
            {
                SignalID = command.SignalID,
                SignalType = command.SignalType,
                Time = command.Time,
                MarginLevel = command.MarginLevel,
                Price = command.Price,
                Quantity = command.Quantity,
                Total = command.Total,
                Balance = command.Balance,
            });


            return _context.SaveChangesAsync(cancellationToken);
        }

        public Task<int> Handle(LogCommand command, CancellationToken cancellationToken)
        {
            _context.Add(new Log()
            {
                Level = command.Level,
                EventId = command.EventId,
                Message = command.Message,
                Time = command.Time,
            });


            return _context.SaveChangesAsync(cancellationToken);
        }
    }
}
