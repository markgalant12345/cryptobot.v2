﻿using CryptoBot.API.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoBot.API.Service
{
    public class QueryHeadlers :
        IRequestHandler<OrdersQuery, OrdersResponse>,
        IRequestHandler<LastOrderQuery, OrderResponse>,
        IRequestHandler<LogsQuery, LogsResponse>
    {
        private readonly DateTime MIN_DATE = new DateTime(2020, 5, 1);

        private readonly CryptoBotContext _context;

        public QueryHeadlers(CryptoBotContext context)
        {
            _context = context;
        }

        public async Task<OrdersResponse> Handle(OrdersQuery request, CancellationToken cancellationToken)
        {
            var orders = await _context.Orders
                .OrderBy(t => t.Time)
                .Where(t => t.Time > MIN_DATE)
                .Select(order => new OrderResponse()
                {
                    Id = order.Id,
                    SignalID = order.SignalID,
                    SignalType = (SignalType)order.SignalType,
                    Time = order.Time,
                    Price = order.Price,
                    MarginLevel = order.MarginLevel,
                    Quantity = order.Quantity,
                    Total = order.Total,
                    Balance = order.Balance,
                }).ToListAsync();


            return new OrdersResponse(orders);
        }

        public async Task<LogsResponse> Handle(LogsQuery request, CancellationToken cancellationToken)
        {
            var logs = await _context.Logs
                .OrderByDescending(t => t.Time)
                .Select(log => new LogResponse()
                 {
                     Id = log.Id,
                     Level = log.Level,
                     EventId = log.EventId,
                     Message = log.Message,
                     Time = log.Time,
                 }).ToListAsync();

            return new LogsResponse(logs);
        }

        public async Task<OrderResponse> Handle(LastOrderQuery request, CancellationToken cancellationToken)
        {
            var order = await _context.Orders
                .OrderByDescending(t => t.Time)
                .Select(it => new OrderResponse()
                {
                    Id = it.Id,
                    SignalID = it.SignalID,
                    SignalType = (SignalType)it.SignalType,
                    Time = it.Time,
                    Price = it.Price,
                    MarginLevel = it.MarginLevel,
                    Quantity = it.Quantity,
                    Total = it.Total,
                    Balance = it.Balance,
                }).FirstOrDefaultAsync();


            return order;
        }

    }
}
