﻿using System.Threading.Tasks;
using CryptoBot.API.Service;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CryptoBot.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<OrdersResponse> Get([FromQuery] OrdersQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("last")]
        public async Task<OrderResponse> Last()
        {
            return await _mediator.Send(new LastOrderQuery());
        }

        [HttpPost]
        public async Task<int> Post([FromBody] CreateOrderCommand command)
        {
           return await _mediator.Send(command);
        }
    }
}
