﻿using System.Threading.Tasks;
using CryptoBot.API.Service;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CryptoBot.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly IMediator _mediator;

        public LogController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<LogsResponse> Get([FromQuery] LogsQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpPost]
        public async Task<int> Post([FromBody] LogCommand command)
        {
            await _mediator.Send(new TelegramCommand(command.Message));

            return await _mediator.Send(command);
        }
    }
}
