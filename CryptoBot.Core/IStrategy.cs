﻿
namespace CryptoBot.Core
{
    public interface IStrategy
    {
        Signal Process(ISource source);
    }
}
