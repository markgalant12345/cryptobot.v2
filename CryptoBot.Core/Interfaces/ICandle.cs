﻿using System;

namespace CryptoBot.Core
{
    public interface ICandle
    {
        /// <summary>
        /// The time this candlestick opened
        /// </summary>
        DateTime OpenTime { get; }
        /// <summary>
        /// The price at which this candlestick opened
        /// </summary>
        decimal Open { get; set; }
        /// <summary>
        /// The highest price in this candlestick
        /// </summary>
        decimal High { get; set; }
        /// <summary>
        /// The lowest price in this candlestick
        /// </summary>
        decimal Low { get; set; }
        /// <summary>
        /// The price at which this candlestick closed
        /// </summary>
        decimal Close { get; set; }
        /// <summary>
        /// The volume traded during this candlestick
        /// </summary>
        decimal Volume { get; set; }
        /// <summary>
        /// The close time of this candlestick
        /// </summary>
        DateTime CloseTime { get; set; }

        /// <summary>
        /// The time this candlestick opened
        /// </summary>
        bool IsFinal { get; set; }
    }
}
