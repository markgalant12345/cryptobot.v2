﻿using System;

namespace CryptoBot.Core
{
    public interface IOrder
    {
        Signal Signal { get; }
        DateTime Time { get; }
        decimal Price { get; }
        decimal MarginLevel { get; }
        decimal Quantity { get; }
        decimal Total { get; }
        decimal Balance { get; }
    }
}
