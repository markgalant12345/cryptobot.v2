﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoBot.Core
{
    public interface ITradePosition
    {
        int ID { get; }

        IOrder EntryOrder { get; }
        IOrder ExitOrder { get; }
    }
}
