﻿namespace CryptoBot.Core
{
    public interface ISymbol
    {
        string Name { get; set; }
        string Market { get; set; }
        string Asset { get; set; }
        decimal QuantityMax { get; set; }
        decimal QuantityMin { get; set; }
        decimal QuantityStep { get; set; }

        string ToString();
    }
}
