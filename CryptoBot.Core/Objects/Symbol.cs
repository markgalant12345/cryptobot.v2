﻿namespace CryptoBot.Core.Objects
{
    public class Symbol :  ISymbol
    {
   
        public string Name { get; set; }
        public string Market { get; set; }
        public string Asset { get; set; }
        public decimal QuantityMax { get; set; }
        public decimal QuantityMin { get; set; }
        public decimal QuantityStep { get; set; }
    }
}
