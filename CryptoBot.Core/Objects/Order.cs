﻿using System;

namespace CryptoBot.Core.Objects
{
    public class Order : IOrder
    {
        public Order()
        {
        }

        public Signal Signal { get; set; }

        public DateTime Time { get; set; }

        public decimal Price { get; set; }
    
        public decimal MarginLevel { get; set; }
      
        public decimal Quantity { get; set; }

        public decimal Total { get; set; }

        public decimal Balance { get; set; }
    }
}
