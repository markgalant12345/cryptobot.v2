﻿namespace CryptoBot.Core.Objects
{
    public class Profit
    {
        public Profit(decimal value, decimal percent)
        {
            Value = value;
            Percent = percent;
        }

        public decimal Value { get; set; }
        public decimal Percent { get; set; }
    }
}
