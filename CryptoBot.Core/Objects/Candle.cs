﻿using System;

namespace CryptoBot.Core.Objects
{
    public class Candle: ICandle
    {
        public  DateTime OpenTime { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public DateTime CloseTime { get; set; }
        public bool IsFinal { get; set; }

        public override string ToString()
        {
            return $"{OpenTime:MM.dd.yy H:mm:ss}  -  {CloseTime:MM.dd.yy H:mm:ss}\nO:{Open:F2}  H:{High:F2}  L:{Low:F2} C:{Close:F2}";
        }
    }
}
