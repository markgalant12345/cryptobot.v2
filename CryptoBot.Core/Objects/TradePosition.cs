﻿namespace CryptoBot.Core.Objects
{
    public class TradePosition : ITradePosition
    {
        public int ID { get; set; }

        public IOrder EntryOrder { get; set; }
        public IOrder ExitOrder { get; set; }
    }
}
