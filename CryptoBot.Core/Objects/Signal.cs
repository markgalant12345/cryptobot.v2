﻿namespace CryptoBot.Core
{
    public enum Signal
    {
        None = 0,
        Long = 1,
        Short = 2,
        Close = 3,
    }
}
