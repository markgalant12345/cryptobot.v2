﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CryptoBot.Core.Objects
{
    public class Source : ISource
    {
        public Source(IList<ICandle> candles, ISymbol symbol, TimeFrame timeFrame)
        {
            Candles = candles;
            Candle = candles[0];
            Symbol = symbol;
            TimeFrame = timeFrame;
        }

        public Source(ISymbol symbol, TimeFrame timeFrame)
        {
            Candles = new List<ICandle>();
            Symbol = symbol;
            TimeFrame = timeFrame;

        }

        public void Add(ICandle candle)
        {
            Candles.Insert(0, candle);
            Candle = candle;
        }

        public void Update(ICandle candle)
        {
            Candles[0] = candle;
            Candle = candle;

            OnChange?.Invoke(this, candle);
        }

        public IEnumerator<ICandle> GetEnumerator()
        {
            return Candles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Candles.GetEnumerator();
        }

        public ISymbol Symbol { get; private set; }

        public IList<ICandle> Candles { get; private set; }

        public ICandle Candle { get; private set; }

        public TimeFrame TimeFrame { get; private set; }

        public EventHandler<ICandle> OnChange { get; set; }
    }
}
