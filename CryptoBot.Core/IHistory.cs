﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoBot.Core
{
    public interface IHistory
    {
        Task<IOrder> Last();
        Task OpenOrder(IOrder order);
        Task CloseOrder(IOrder order);
    }
}
