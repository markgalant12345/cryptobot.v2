﻿using System.Threading.Tasks;

namespace CryptoBot.Core
{
    public interface ISourceManager
    {
        Task<ISource> CreateSourceAsync(string symbolName, TimeFrame timeFrame);
    }
}