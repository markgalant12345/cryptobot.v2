﻿using System.Threading;
using System.Threading.Tasks;

namespace CryptoBot.Core
{
    public interface IBroker
    {
        IHistory History { get; }
        Task ExecudeSignal(Signal signal, ISource source, CancellationToken token = default(CancellationToken));
    }
}
