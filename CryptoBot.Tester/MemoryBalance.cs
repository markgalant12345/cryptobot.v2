﻿using System.Collections.Generic;

namespace CryptoBot.Tester
{
    public class MemoryBalance : Dictionary<string, decimal>
    {
        public static Dictionary<string, decimal> INIT = new Dictionary<string, decimal>()
        {
            { "ETH", 0 },
            { "USDT", 1000 }
        };

        public MemoryBalance() : base(INIT)
        {
        }
    }
}
