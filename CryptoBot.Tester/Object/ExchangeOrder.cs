﻿using Binance.Net.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoBot.Tester.Object
{
    class ExchangeOrder
    {

        //
        // Summary:
        public DateTime Time { get; set; }
        //
        // Summary:
        //     The iceberg quantity
        public decimal IcebergQuantity { get; set; }
        //
        // Summary:
        //     The stop price
        public decimal StopPrice { get; set; }
        //
        // Summary:
        //     The side of the order
        public OrderSide Side { get; set; }
        //
        // Summary:
        //     The type of the order
        public OrderType Type { get; set; }
        //
        // Summary:
        //     How long the order is active
        public TimeInForce TimeInForce { get; set; }
        //
        // Summary:
        //     The status of the order
        public OrderStatus Status { get; set; }
        //
        // Summary:
        //     The original quote order quantity
        public decimal OriginalQuoteOrderQuantity { get; set; }
        //
        // Summary:
        //     Cummulative amount
        public decimal CummulativeQuoteQuantity { get; set; }
        //
        // Summary:
        //     The currently executed quantity of the order
        public decimal ExecutedQuantity { get; set; }
        //
        // Summary:
        //     The original quantity of the order
        public decimal OriginalQuantity { get; set; }
        //
        // Summary:
        //     The price of the order
        public decimal Price { get; set; }
        //
        // Summary:
        //     The order id as assigned by the client
        public string ClientOrderId { get; set; }
        //
        // Summary:
        //     The order id generated by Binance
        public long OrderId { get; set; }
        //
        // Summary:
        //     The symbol the order is for
        public string Symbol { get; set; }
        //
        // Summary:
        //     The time the order was last updated
        public DateTime UpdateTime { get; set; }
        //
        // Summary:
        //     Is working
        public bool IsWorking { get; set; }
    }
}
