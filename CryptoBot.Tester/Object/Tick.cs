﻿using System;

namespace CryptoBot.Tester.Object
{
    public class Tick
    {
        public  DateTime Time { get; set; }
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
    }
}
