﻿namespace CryptoBot.Tester.Object
{
    public enum SideEffect
    {  //
        // Summary:
        //     Normal trade
        NoSideEffect = 0,
        //
        // Summary:
        //     Margin trade order
        MarginBuy = 1,
        //
        // Summary:
        //     Make auto repayment after order is filled
        AutoRepay = 2
    }
}
