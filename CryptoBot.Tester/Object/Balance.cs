﻿namespace CryptoBot.Tester.Object
{
    public class Balance
    {
        // Summary:
        //     The asset this balance is for
        public string Asset { get; set; }
        //
        // Summary:
        //     The amount that was borrowed
        public decimal Borrowed { get; set; }
        //
        // Summary:
        //     The amount that isn't locked in a trade
        public decimal Free { get; set; }
        //
        // Summary:
        //     Commission to need pay by borrowed
        public decimal Interest { get; set; }
        //
        // Summary:
        //     The amount that is currently locked in a trade
        public decimal Locked { get; set; }
        //
        // Summary:
        //     The total balance of this asset (Free + Locked)
        public decimal Total { get; }
    }
}
