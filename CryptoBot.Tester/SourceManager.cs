﻿using Binance.Net.Interfaces;
using Binance.Net.Objects;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoBot.Tester
{
    public class SourceManager : ISourceManager
    {
        private IBinanceClient _client;
        private TimeFrame _timeFrame;
        private ISymbol _symbol;
        private IList<ICandle> _klines;

        public SourceManager(IBinanceClient binanceClient)
        {
            _client = binanceClient;
        }


        public async Task<ISource> CreateSourceAsync(string symbolName, TimeFrame timeFrame)
        {
            _timeFrame = timeFrame;
            _symbol = await GetSymbol(symbolName);
            _klines = await GetKlinesAsync(symbolName, timeFrame);

            return await Get(0);
        }

        public Task<ISource> Get(int index)
        {
            var klines = _klines.Skip(index).ToList();
            var source = new Source(klines, _symbol, _timeFrame);

            return Task.FromResult<ISource>(source);
        }

        private async Task<IList<ICandle>> GetKlinesAsync(string symbol, TimeFrame timeFrame)
        {
            var data = new List<BinanceKline>();
            var startTime = DateTime.Parse("2016-01-01");
            while (true)
            {

                var klines = await _client.GetKlinesAsync(symbol, (KlineInterval)timeFrame, startTime: startTime, limit: 1000);

                if (klines.Data.Count() == 1)
                    return data.OrderByDescending(t => t.OpenTime).Select(Convert).Skip(1).ToList();

                startTime = klines.Data.Select(t => t.OpenTime).Max();

                data.AddRange(klines.Data);
            }
        }

        private async Task<ISymbol> GetSymbol(string symbol)
        {
            var info = await _client.GetExchangeInfoAsync();
            var item = info.Data.Symbols.FirstOrDefault(t => t.Name == symbol);

            return new Symbol()
            {
                Name = symbol,
                Market = item.QuoteAsset,
                Asset = item.BaseAsset,
                QuantityMax = item.LotSizeFilter.MaxQuantity,
                QuantityMin = item.LotSizeFilter.MinQuantity,
                QuantityStep = item.LotSizeFilter.StepSize,
            };
        }

        private ICandle Convert(BinanceKline kline)
        {
            return new Candle()
            {
                OpenTime = kline.OpenTime,
                Open = kline.Open,
                High = kline.High,
                Low = kline.Low,
                Close = kline.Close,
                CloseTime = kline.CloseTime,
                Volume = kline.Volume,
            };
        }
    }
}
