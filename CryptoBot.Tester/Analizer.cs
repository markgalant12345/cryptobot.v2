﻿using Binance.Net;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Strategies;
using CryptoBot.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoBot.Tester
{
    public class Analizer
    {
        public ISource _source;
        private SourceManager _sourceManager;
        private DateTime _startTime;
        private DateTime _endTime;

        public Analizer()
        {
            var client = new BinanceClient();
            _sourceManager = new SourceManager(client);
            _startTime = DateTime.Parse("2020-01-1");
            _endTime = DateTime.Parse("2020-02-1");

        }

        public async Task Run()
        {
            _source = await _sourceManager.CreateSourceAsync("ETHUSDT", TimeFrame.OneHour);


            var inputs = CreateInputs();

            var results = new List<AnalizeResult>();

            for (var i = 0; i < inputs.Count(); i++)
            {
                Console.Clear();
                Console.WriteLine($"{i}/{inputs.Count()}");

                results.Add(await Analize(inputs[i]));
            }

            PrintResults(results);
        }

        private List<AnalizeInput> CreateInputs()
        {
            var inputs = new List<AnalizeInput>();

            var step = 5;
            for (var l = step; l < 100; l += step)
                for (var s = l+1; s < 100; s += step)
                    inputs.Add(new AnalizeInput(14, -57m, -57m, l, s));

            return inputs;
        }

        private void PrintResults(IEnumerable<AnalizeResult> results)
        {
            Console.Clear();

            foreach (var result in results.OrderBy(t => t.Perift))
            {
                Console.WriteLine("{0:P} {1} | {2}", result.Perift, result.CountOrders, result.Input);
            }
        }
        public  Task<AnalizeResult> Analize(AnalizeInput input)
        {
            //var broker = new MemoryBroker();
            //var strategy = new WilliamsMarkusStrategy(input.AcSlow, input.AcFast, input.RLenght, input.LontRTake, input.ShortRTake );

            //var count = _source.Count();

            //for (var i = count - 1; i > 1; i--)
            //{
            //    var source = await _sourceManager.Get(i);

            //    if (!(source.Candle.OpenTime >= _startTime && source.Candle.OpenTime < _endTime))
            //        continue;

            //    var signal = strategy.Process(source);

            //    if (signal != null)
            //        await broker.ExecudeSignal(signal, source);
            //}


            //var history = ((MemoryHistory)broker.History);

            //return new AnalizeResult()
            //{
            //    Input = input,
            //    Perift = history.Perift,
            //    CountOrders = history.Count,

            //};
            return null;
        }

        public class AnalizeInput
        {
            public AnalizeInput(int r, decimal l, decimal s, int af, int asl)
            {
                RLenght = r;
                LontRTake = l;
                ShortRTake = s;
                AcFast = af;
                AcSlow = asl;
            }

            public int RLenght { get; set; }
            public int AcFast { get; set; }
            public int AcSlow { get; set; }
            public decimal LontRTake { get; set; }
            public decimal ShortRTake { get; set; }

            public override string ToString()
            {
                return $"s:{ShortRTake:F2} l:{LontRTake:F2} r:{RLenght} aS:{AcSlow} aF:{AcFast}";
            }
        }

        public class AnalizeResult
        {
            public AnalizeInput Input { get; set; }
            public decimal Perift { get; set; }
            public int CountOrders { get; set; }
        }
    }
}
