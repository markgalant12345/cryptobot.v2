﻿using System;
using System.Threading.Tasks;

namespace CryptoBot.Tester
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var analizer = new Analizer();

            await analizer.Run();
        }
    }
}
