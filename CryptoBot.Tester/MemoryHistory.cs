﻿using CryptoBot.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoBot.Test
{
    public class MemoryHistory : IHistory
    {
        public List<IOrder> Orders;

        public MemoryHistory()
        {
            Orders = new List<IOrder>();
        }


        public Task CloseOrder(IOrder order)
        {
            Orders.Add(order);

            return Task.CompletedTask;
        }

        public Task<IOrder> Last()
        {
            return Task.FromResult(Orders.LastOrDefault());
        }

        public Task OpenOrder(IOrder order)
        {
            Orders.Add(order);

            return Task.CompletedTask;
        }

        public decimal Perift => Orders[Orders.Count -1].Balance / Orders[0].Balance - 1.0m;
        public int Count => Orders.Count;
    }
}
