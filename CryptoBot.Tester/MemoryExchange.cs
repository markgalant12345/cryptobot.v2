﻿using Binance.Net.Objects;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Tester.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OrderSide = CryptoBot.Tester.Object.OrderSide;

namespace CryptoBot.Tester
{
    public class MemoryExchange
    {
        private Dictionary<TimeFrame, int> _timeFrames;
        private Queue<Tick> _ticks;
        private Tick _current;

        public Dictionary<TimeFrame, Source> Sources { get; set; }

        public IEnumerable<Balance> Balance { get; set; }
        public IEnumerable<Order> Orders { get; set; }

        public MemoryExchange(IEnumerable<Tick> ticks, IEnumerable<Balance> balance)
        {
            Orders = new List<Order>();
            Balance = balance;
            _ticks = new Queue<Tick>(ticks);
            _timeFrames = new Dictionary<TimeFrame, int>()
            {
                { TimeFrame.OneMinute, 1 },
                { TimeFrame.FiveMinutes, 5 },
                { TimeFrame.FifteenMinutes, 15 },
                { TimeFrame.OneHour, 60 },
                { TimeFrame.FourHour, 240 },
            };

            Sources = _timeFrames.ToDictionary(it => it.Key, it => new Source(null, it.Key));
        }


        public bool EmulateNextTick()
        {
            _current = _ticks.Dequeue();

            foreach (var timeFrame in _timeFrames)
                UpdateSource(_current, timeFrame.Key, timeFrame.Value);


            return _ticks.Count != 0;
        }

        public void UpdateSource(Tick tick, TimeFrame timeFrame, int count)
        {
            var last = Sources[timeFrame].Candle;

            if (last == null || tick.Time > last.OpenTime.AddMinutes(count))
            {

                if (last != null)
                {
                    last.IsFinal = true;
                    last.CloseTime = MinuteClear(last.OpenTime).AddMinutes(count).AddTicks(-10000);
                    Sources[timeFrame].Update(last);
                }


                Sources[timeFrame].Add(new Candle()
                {
                    OpenTime = MinuteClear(tick.Time),
                    Open = tick.Price,
                    High = tick.Price,
                    Low = tick.Price,
                    Volume = tick.Volume,
                    IsFinal = false
                });

                return;
            }


            Sources[timeFrame].Update(new Candle()
            {
                OpenTime = last.OpenTime,
                Open = last.Open,
                High = Math.Max(last.High, tick.Price),
                Low = Math.Min(last.Low, tick.Price),
                Close = tick.Price,
                Volume = last.Volume + tick.Volume,
                IsFinal = false,
            });
        }

        public async Task PlaceOrderAsync(OrderSide side, ISymbol symbol, decimal quantity, SideEffectType type, CancellationToken token)
        {
            var price = _current.Price;

            if (side == OrderSide.Buy && type == SideEffectType.MarginBuy)
            {
                var balance = GetBalance(symbol.Market);
                var order = new Order()
                {

                };
            }



            //if (!result.Success)
            //{
            //    await _log.LogCritical(MsgFactory.PlaceOrderError(result.Error.Message));

            //    return null;
            //}

        }

        private Balance GetBalance(string asset)
        {
            return Balance.FirstOrDefault(it => it.Asset == asset);
        }
        private DateTime MinuteClear(DateTime time)
        {

            return new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, 0, DateTimeKind.Utc);
        }

        public Task<decimal> GetPriceAsync(string symbol, CancellationToken ct = default)
        {
            return Task.FromResult(_current.Price);
        }
    }
}
