import Vue from 'vue'
import axios from 'axios'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'

import App from './App.vue'
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';

Vue.use(ElementUI, { locale })

Vue.config.productionTip = false

axios.defaults.baseURL = process.env.VUE_APP_API_URL || 'http://128.199.50.26:5000/api/';

new Vue({
  render: h => h(App),
}).$mount('#app')
