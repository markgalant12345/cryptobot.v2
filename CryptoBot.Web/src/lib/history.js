export const emprtyPrefit = {
  percent: 0,
  value: 0,
};

export const calcPrefit = (prev, curr) => ({
  percent: ((prev * 1.0) / (curr * 1.0) - 1.0) * 100,
  value: prev - curr
});

export const calcItemPrefit = (current, previus) => {
  if (previus && previus.signalType === 1)
    return calcPrefit(current.price, previus.price);

  if (previus && previus.signalType === 2)
    return calcPrefit(previus.price, current.price);

  return emprtyPrefit;
};

export const calcItemBalance = (current, previus) => {
  if (previus && previus.signalType !== 3)
    return calcPrefit(current.balance, previus.balance);

  return emprtyPrefit;
};

export const buildHistory = (orders) => {
  return orders.map((item, idx, arr) => ({
    ...item,
    prefit: calcItemPrefit(item, arr[idx - 1]),
    balancePrefit: calcItemBalance(item, arr[idx - 1]),
  }))
}


export const percentPrefit = (history) => {
  if (history.length <= 0)
    return emprtyPrefit;

  const percent = history.reduce(
    (total, it) => total + it.prefit.percent,
    0
  );
  const value = history.reduce(
    (total, it) => total + it.prefit.value,
    0
  );

  return {
    percent,
    value
  };
};

export const netPrefit = (history) => {
  if (history.length <= 0)
    return emprtyPrefit;


    const percent = history.reduce(
      (total, it) => total + it.balancePrefit.percent,
      0
    );
    const value = history.reduce(
      (total, it) => total + it.balancePrefit.value,
      0
    );
  
    return {
      percent,
      value
    };
};
export const percentDrawdown = (history) => {
  if (history.length <= 0)
    return emprtyPrefit;

  const percent = history.reduce(
    (max, it) => Math.min(max, it.prefit.percent),
    0
  );
  const value = history.reduce(
    (max, it) => Math.min(max, it.prefit.value),
    0
  );

  return {
    percent,
    value
  };
};


export const netDrawdown = (history) => {
  if (history.length <= 0)
    return emprtyPrefit;

  const percent = history.reduce(
    (max, it) => Math.min(max, it.balancePrefit.percent),
    0
  );
  const value = history.reduce(
    (max, it) => Math.min(max, it.balancePrefit.value),
    0
  );

  return {
    percent,
    value
  };
};

