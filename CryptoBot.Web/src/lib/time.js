import Vue from "vue";
import moment from "moment";

Vue.filter('format-date', function (date) {
  return moment.utc(date).format('MM.DD.YYYY HH:mm:ss')
})

Vue.filter('moment-ago', function (date) {
  return moment.utc(date).fromNow()
})

Vue.filter('usd-price', function (value) {
  return value.toFixed(2);
})

Vue.filter('crypto-price', function (value) {
  return value.toFixed(4);
})