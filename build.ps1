echo "Strat "

# Bot
docker image build -t crypto-bot-bot . -f ./CryptoBot/Dockerfile
docker tag crypto-bot-bot  registry.gitlab.com/markgalant12345/cryptobot.v2/bot
docker push registry.gitlab.com/markgalant12345/cryptobot.v2/bot

# API
docker image build -t crypto-bot-api . -f ./CryptoBot.API/Dockerfile
docker tag crypto-bot-api  registry.gitlab.com/markgalant12345/cryptobot.v2/api
docker push registry.gitlab.com/markgalant12345/cryptobot.v2/api

# Aggregator
# docker image build -t crypto-bot-aggregator . -f ./CryptoBot.Aggregator/Dockerfile
# docker tag crypto-bot-aggregator  registry.gitlab.com/markgalant12345/cryptobot.v2/aggregator
# docker push registry.gitlab.com/markgalant12345/cryptobot.v2/aggregator


# Web
cd ./CryptoBot.Web
docker image build -t crypto-bot-web .
docker tag crypto-bot-web  registry.gitlab.com/markgalant12345/cryptobot.v2/web
docker push registry.gitlab.com/markgalant12345/cryptobot.v2/web
cd ..

echo "End "