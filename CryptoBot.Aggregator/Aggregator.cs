﻿using Binance.Net.Interfaces;
using Binance.Net.Objects;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoBot.Aggregator
{
    public class Aggregator : IHostedService
    {
        private IBinanceSocketClient _socketClient;
        private string[] _symbols;

        public Aggregator(IBinanceSocketClient socketClient)
        {
            _socketClient = socketClient;
            _symbols = new string[] { "ETHUSDT", "BTCUSDT", "BNBUSDT" };

        }


        public async Task StartAsync(CancellationToken cancellationToken)
        {
           foreach(var symbol in _symbols)
            {
                await CreateStream(symbol);
            }
        }


        private async Task CreateStream(string symbol)
        {
            Directory.CreateDirectory($"./data/{symbol.ToLower()}/");

            await _socketClient.SubscribeToPartialOrderBookUpdatesAsync(symbol.ToLower(), 20, 1000, OnDeepChange);
        }

        private async void OnDeepChange(BinanceOrderBook deep)
        {
            var json = JsonConvert.SerializeObject(deep);
            await Save(deep.Symbol, json);
        }

        private async Task Save(string symbol, string json)
        {
            await File.AppendAllTextAsync($"./data/{symbol}/{DateTime.Now:dd-MM-yy}.data", json);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
