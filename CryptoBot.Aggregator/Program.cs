﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Globalization;
using System.IO;

namespace CryptoBot.Aggregator
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US"); ;

            var builder = new HostBuilder();
            builder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                var environmentName = hostingContext.HostingEnvironment.EnvironmentName;
                Console.WriteLine($"Env {environmentName}");

                config.SetBasePath(Directory.GetCurrentDirectory());
                config.AddJsonFile("appsettings.json", true);
                config.AddJsonFile($"appsettings.{environmentName}.json", true);

                if (args != null)
                {
                    config.AddCommandLine(args);
                }
            })
            .ConfigureServices(Startup.ConfigureServices)
            .ConfigureLogging(Startup.ConfigureLogging)
            .UseConsoleLifetime();

            builder.RunConsoleAsync().Wait();
        }
    }
}
