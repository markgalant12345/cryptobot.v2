﻿using CryptoExchange.Net.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoBot.Test.Mocks
{
    public class MockError : Error
    {
        public MockError(int code, string message, object data) : base(code, message, data)
        {
        }
    }
}
