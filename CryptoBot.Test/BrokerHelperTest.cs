﻿using CryptoBot.Core.Objects;
using CryptoBot.Helpers;
using CryptoBot.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace CryptoBot.Test
{
    [TestClass]
    public class BrokerHelperTest
    {
        [TestMethod]
        public void NormalizQShudBeLowerQ()
        {
            var sumol = new Symbol()
            {
                Asset = "ETH",
                Market = "USDT",
                Name = "ETHUSDT",
                QuantityMax = 9000.0m,
                QuantityMin = 0.00001m,
                QuantityStep = 0.00001m,
            };

            var quantity = 411.11333333m / 201.54m;
            var normalizQ = BrokerHelper.NormalizQ(sumol, quantity);

            Assert.IsTrue(normalizQ <= quantity);
        }
    }
}
