using Binance.Net;
using Binance.Net.Objects;
using CryptoBot.Binance;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Tester;
using CryptoBot.Tester.Object;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CryptoBot.Test
{
    [TestClass]
    public class ExchangeTest
    {
        public List<Tick> Ticks = new List<Tick>() {
                new Tick()
                {
                    Time =  DateTime.Parse("2019-01-01T00:00:02.081"),
                    Price = 131.45000000m,
                    Volume = 0.80000000m,
                },
                new Tick()
                {
                    Time =  DateTime.Parse("2019-01-01T00:00:02.083"),
                    Price = 131.44000000m,
                    Volume = 4.20000000m,
                },
                new Tick()
                {
                    Time =  DateTime.Parse("2019-01-01T00:00:02.558"),
                    Price = 131.46000000m,
                    Volume = 4.35600000m,
                },
            };

        private IEnumerable<ICandle> GetCandles(string symbol, TimeFrame timeFrame, DateTime start, DateTime end)
        {
            var bianceClient = new BinanceClient();
            var result = bianceClient.GetKlines(symbol, (KlineInterval)timeFrame, startTime: start, endTime: end.AddSeconds(-1), limit: 1000);

            return result.Data.Select(kline => new Candle()
            {
                OpenTime = kline.OpenTime,
                Open = kline.Open,
                High = kline.High,
                Low = kline.Low,
                Close = kline.Close,
                CloseTime = kline.CloseTime,
                Volume = kline.Volume,
                IsFinal = true,
            }).OrderByDescending(t => t.OpenTime);
        }

        private IEnumerable<Tick> GetTicks(string symbol, DateTime start, DateTime end)
        {
            var bianceClient = new BinanceClient();
            var ticks = new List<Tick>();

            for (var hour = 0; hour <= (end - start).TotalHours; hour++)
            {
                var result = bianceClient.GetAggregatedTrades(symbol, startTime: start.AddHours(hour), endTime: start.AddHours(hour + 1));
                ticks = ticks.Concat(result.Data.Select(tick => new Tick()
                {
                    Time = tick.Timestamp,
                    Price = tick.Price,
                    Volume = tick.Quantity,
                })).ToList();
            }

            return ticks.OrderBy(t => t.Time).Where(t => t.Time >= start && t.Time < end.AddSeconds(10));
        }

        [TestMethod]
        [DataRow(TimeFrame.OneMinute)]
        [DataRow(TimeFrame.FiveMinutes)]
        [DataRow(TimeFrame.FifteenMinutes)]
        [DataRow(TimeFrame.OneHour)]
        [DataRow(TimeFrame.FourHour)]
        public void Test_Candles_All_Ticks(TimeFrame timeFrame)
        {
            var symbol = "ETHUSDT";
            var start = new DateTime(2019, 1, 1);
            var end = new DateTime(2019, 1, 1, 8, 0, 0);
            var ticks = GetTicks(symbol, start, end);

            var expected = GetCandles(symbol, timeFrame, start, end).ToArray();

            var memoryExchange = new MemoryExchange(ticks, new List<Balance>());
            while (memoryExchange.EmulateNextTick()) ;

            var actual = memoryExchange.Sources[timeFrame].Candles.Where(t => t.IsFinal).ToArray();

            var s = JsonConvert.SerializeObject(ticks);
            var expectedJson = JsonConvert.SerializeObject(expected);
            var actualJson = JsonConvert.SerializeObject(actual);

            Assert.AreEqual(expected.Count(), actual.Count());
            Assert.AreEqual(expectedJson, actualJson);
            Debug.WriteLine("TimeFrame " + Enum.GetName(typeof(TimeFrame), timeFrame));
        }

        [TestMethod]
        [DataRow(TimeFrame.OneMinute)]
        [DataRow(TimeFrame.FiveMinutes)]
        [DataRow(TimeFrame.FifteenMinutes)]
        [DataRow(TimeFrame.OneHour)]
        [DataRow(TimeFrame.FourHour)]
        public void Test_Ecah_Ticks(TimeFrame timeFrame)
        {
            var memoryExchange = new MemoryExchange(Ticks, new List<Balance>());
            memoryExchange.EmulateNextTick();
            memoryExchange.EmulateNextTick();

            memoryExchange.Sources[timeFrame].OnChange += (sender, actual) =>
            {
                var expected = new Candle()
                {
                    OpenTime = new DateTime(2019, 1, 1, 0, 0, 0, kind: DateTimeKind.Utc),
                    IsFinal = false,
                    Open = 131.45000000m,
                    High = 131.46000000m,
                    Low = 131.44000000m,
                    Close = 131.46000000m,
                    Volume = 9.35600000m
                };
                Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
            };

            memoryExchange.EmulateNextTick();
        }

        [TestMethod]
        public void Test_Price()
        {
            var memoryExchange = new MemoryExchange(Ticks, new List<Balance>());
            memoryExchange.EmulateNextTick();

            Assert.AreEqual(131.45m, memoryExchange.GetPriceAsync("ETHUSDT").Result);

            memoryExchange.EmulateNextTick();

            Assert.AreEqual(131.44m, memoryExchange.GetPriceAsync("ETHUSDT").Result);

            memoryExchange.EmulateNextTick();

            Assert.AreEqual(131.46m, memoryExchange.GetPriceAsync("ETHUSDT").Result);

        }
    }
}
