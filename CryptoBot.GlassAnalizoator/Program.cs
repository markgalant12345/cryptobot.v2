﻿using Microsoft.Extensions.Hosting;
using System;
using System.Globalization;

namespace CryptoBot.GlassAnalizoator
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US"); ;

            var builder = new HostBuilder()
            .ConfigureServices(Startup.ConfigureServices)
            .ConfigureLogging(Startup.ConfigureLogging)
            .UseConsoleLifetime();

            builder.RunConsoleAsync().Wait();
        }
    }
}
