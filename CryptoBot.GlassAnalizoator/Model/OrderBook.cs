﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CryptoBot.GlassAnalizoator.Model
{
    public class OrderBook
    {
        //
        // Summary:
        //     The ID of the last update
        [JsonProperty("lastUpdateId")]
        public long LastUpdateId { get; set; }
        //
        // Summary:
        //     The list of bids
        public IEnumerable<OrderBookItem> Bids { get; set; }
        //
        // Summary:
        //     The list of asks
        public IEnumerable<OrderBookItem> Asks { get; set; }

        public decimal AskQ { get; set; }
        public decimal AskC { get; set; }
        public decimal BidQ { get; set; }
        public decimal BidC { get; set; }
        public decimal Bid => Bids.First().Price;
        public decimal Ask => Asks.First().Price;
    }
}
