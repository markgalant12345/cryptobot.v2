﻿using Newtonsoft.Json;

namespace CryptoBot.GlassAnalizoator.Model
{
    [JsonConverter(typeof(ArrayConverter))]
    public class OrderBookItem
    {
        //
        // Summary:
        //     The price of this order book entry
        [ArrayProperty(0)]

        public decimal Price { get; set; }
        //
        // Summary:
        //     The quantity of this price in the order book
        [ArrayProperty(1)]
        public decimal Quantity { get; set; }

        [JsonIgnore]
        public decimal PQuantity { get; set; }
    }
}
