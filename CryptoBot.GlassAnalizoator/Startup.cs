﻿using CryptoBot.GlassAnalizoator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CryptoBot
{
    public static class Startup
    {
        // Use this method to add services to the container.
        public static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            services.AddSingleton<IHostedService, Analizoator>();
            // Binace 
            //services.AddTransient(ConfigureBinanceSocketClient);
        }

        // Use this method to configure the Loggin.
        public static void ConfigureLogging(HostBuilderContext context, ILoggingBuilder logging)
        {
            logging.AddConfiguration(context.Configuration.GetSection("Logging"));
            logging.AddConsole();
        }

        //private static IBinanceClient ConfigureBinanceClient(IServiceProvider provider)
        //{
        //    var credentials = provider.GetService<ApiCredentials>();
        //    var clientOptions = new BinanceClientOptions()
        //    {
        //        ApiCredentials = credentials,
        //        LogVerbosity = LogVerbosity.Info,
        //        LogWriters = new List<TextWriter> { Console.Out }
        //    };

        //    return new BinanceClient(clientOptions);
        //}

        //private static IBinanceSocketClient ConfigureBinanceSocketClient(IServiceProvider provider)
        //{
        //    var options = new BinanceSocketClientOptions()
        //    {
        //        LogVerbosity = LogVerbosity.Info,
        //        LogWriters = new List<TextWriter> { Console.Out }
        //    };

        //    return new BinanceSocketClient(options);
        //}
    }
}
