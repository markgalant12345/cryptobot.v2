﻿using CryptoBot.GlassAnalizoator.Model;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using System.IO;

namespace CryptoBot.GlassAnalizoator
{
    public class Analizoator : IHostedService
    {
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var depth = await Download("ethusdt", "02-04-20");
            var deviated = CalculateDeviation(depth);
            var topAsks = GetTopAsksOrders(deviated, 5);
            var topBids = GetTopBidsOrders(deviated, 5);

            Console.WriteLine("Top Ask count {0}", topAsks.Count());
            Console.WriteLine("Top Bid count {0}", topBids.Count());

            var x = deviated.FirstOrDefault(t => t.AskQ < 0.20m);
            RenderChanges(deviated.ToList(), x);
        }

        public void RenderChanges(List<OrderBook> orders, OrderBook from)
        {
            var index = orders.FindIndex(t => t.LastUpdateId == from.LastUpdateId);
            foreach (var order in orders.GetRange(index - 5 ,50))
            {
                PrintBookOrder(order);
            }
        }

        protected void PrintBookOrder(OrderBook book)
        {
            Console.WriteLine("#{0} B:{1:F2} A:{2:F2}", book.LastUpdateId, book.BidQ, book.AskQ);
            var index = 20;
            foreach (var ask in book.Asks.Reverse())
            {
                Console.WriteLine(string.Format("{0,2} {1,6:C} {2,6:F2} {3,10:F2}", index--, ask.Price, ask.Quantity, ask.PQuantity));
            }

            Console.WriteLine("---------------------------");

            index = 1;
            foreach (var bid in book.Bids.Reverse())
            {
                Console.WriteLine(string.Format("{0,2} {1,6:C} {2,6:F2} {3,10:F2}", index++, bid.Price, bid.Quantity, bid.PQuantity));
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }

        public IList<OrderBook> GetTopBidsOrders(IEnumerable<OrderBook> orders, decimal minQPercent)
        {
            var result = new List<OrderBook>();
            foreach(var order in orders)
            {
                var bid = order.Bids.FirstOrDefault(b => b.PQuantity > minQPercent);

                if (bid == order.Bids.Last() && !result.Any(t => t.Bids.Any(a => a.Price == bid.Price)))
                    result.Add(order);
            }

            return result;
        }

        public IList<OrderBook> GetTopAsksOrders(IEnumerable<OrderBook> orders, decimal minQPercent)
        {
            var result = new List<OrderBook>();
            foreach (var order in orders)
            {
                var ask = order.Asks.FirstOrDefault(b => b.PQuantity > minQPercent);

                if (ask != null && !result.Any(t => t.Bids.Any(a => a.Price == ask.Price)))
                    result.Add(order);
            }

            return result;
        }

        public IList<OrderBook> CalculateDeviation(IEnumerable<OrderBook> orders)
        {
            OrderBook prev = null;
            foreach (var order in orders)
            {
                var Aavg = order.Asks.Average(t => t.Quantity);

                foreach (var ask in order.Asks)
                {
                    ask.PQuantity = ask.Quantity / Aavg;
                }

                var Bavg = order.Bids.Average(t => t.Quantity);

                foreach (var bid in order.Bids)
                {
                    bid.PQuantity = bid.Quantity / Bavg;
                }

                var sum = order.Asks.Concat(order.Bids).Sum(t => t.Quantity);
                order.AskQ = order.Asks.Sum(t => t.Quantity) / sum;
                order.BidQ = order.Bids.Sum(t => t.Quantity) / sum;

                if(prev != null)
                {
                    order.AskC = order.Ask - prev.Ask;
                    order.BidC = order.Bid - prev.Bid;
                }
                
                prev = order;
            }

            return orders.ToList();
        }

        private async Task<IEnumerable<OrderBook>> Download(string symbol, string date)
        {
            var content = await File.ReadAllTextAsync($"./Data/{symbol}/{date}.data");
            var orders = JsonConvert.DeserializeObject<IEnumerable<OrderBook>>($"[{content.Replace("}{", "},{")}]");
            return orders.OrderBy(t => t.LastUpdateId);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
