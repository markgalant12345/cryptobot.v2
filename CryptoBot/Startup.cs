﻿using Binance.Net;
using Binance.Net.Interfaces;
using Binance.Net.Objects;
using CryptoBot.API;
using CryptoBot.Binance;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Objects;
using CryptoBot.Strategies;
using CryptoExchange.Net.Authentication;
using CryptoExchange.Net.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rest.Net;
using Rest.Net.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace CryptoBot
{
    public static class Startup
    {
        // Use this method to add services to the container.
        public static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            var API_URL = context.Configuration.GetValue<string>("API:URL");
            var BINANCE_API_KEY = context.Configuration.GetValue<string>("Binance:Key");
            var BINANCE_API_SECRET = context.Configuration.GetValue<string>("Binance:Secret");
            var brokerSettings = context.Configuration.GetSection("Settings").Get<BrokerSettings>();

            services.AddTransient(x => new ApiCredentials(BINANCE_API_KEY, BINANCE_API_SECRET));

            services.AddTransient<IOrder, Order>();
            services.AddTransient<IHistory, HistoryApi>();
            services.AddTransient<LoggerApi, LoggerApi>();
            services.AddTransient<IRestClient>((cx) => new RestClient(API_URL));
            services.AddSingleton<IHostedService, Bot>();

            // Binace 
            services.AddTransient(ConfigureBinanceClient);
            services.AddTransient(ConfigureBinanceSocketClient);
            services.AddTransient((cx) => brokerSettings);
            services.AddTransient<ISourceManager, BinanceSourceManager>();
            services.AddTransient<IBroker, BinanceMarginBroker>();
        }

        // Use this method to configure the Loggin.
        public static void ConfigureLogging(HostBuilderContext context, ILoggingBuilder logging)
        {
            logging.AddConfiguration(context.Configuration.GetSection("Logging"));
            logging.AddConsole();
        }

        private static IBinanceClient ConfigureBinanceClient(IServiceProvider provider)
        {
            var credentials = provider.GetService<ApiCredentials>();
            var clientOptions = new BinanceClientOptions()
            {
                ApiCredentials = credentials,
                LogVerbosity = LogVerbosity.Info,
                LogWriters = new List<TextWriter> { Console.Out }
            };

            return new BinanceClient(clientOptions);
        }

        private static IBinanceSocketClient ConfigureBinanceSocketClient(IServiceProvider provider)
        {
            var credentials = provider.GetService<ApiCredentials>();
            var options = new BinanceSocketClientOptions()
            {
                ApiCredentials = credentials,
                LogVerbosity = LogVerbosity.Info,
                LogWriters = new List<TextWriter> { Console.Out }
            };

            return new BinanceSocketClient(options);
        }
    }
}
