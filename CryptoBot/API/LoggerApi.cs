﻿
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Rest.Net.Interfaces;
using System;
using System.Threading.Tasks;

namespace CryptoBot.API
{
    public class LoggerApi
    {
        private readonly IRestClient _restClient;

        public readonly ILogger<LoggerApi> _logger;

        public LoggerApi(IRestClient restClient, ILogger<LoggerApi> logger)
        {
            _restClient = restClient;
            _logger = logger;
        }

        private async Task Log(LogLevel level, int eventId, string message)
        {
            var command = new
            {
                level,
                eventId,
                time = DateTime.Now,
                message,
            };

            _logger.LogDebug("LoggerApi | {0}", JsonConvert.SerializeObject(command));

//#if (DEBUG)
//            return;
//#endif

            var result = await _restClient.PostAsync("/api/log", command, false);

            if (result.IsError)
            {
              _logger.LogDebug($"LoggerApi | Server error {result.Data}");
            }
        }

        public async Task LogDebug(string message)
        {
            await Log(LogLevel.Debug, 1, message);
        }

        public async Task LogError(string message)
        {
            await Log(LogLevel.Error, 1, message);
        }

        public async Task LogInformation(string message)
        {
            await Log(LogLevel.Information, 1, message);
        }

        public async Task LogWarning(string message)
        {
            await Log(LogLevel.Warning, 1, message);
        }

        public async Task LogCritical(string message)
        {
            await Log(LogLevel.Critical, 1, message);
        }
    }
}
