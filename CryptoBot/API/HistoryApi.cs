﻿using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Objects;
using CryptoBot.Strategies;
using Newtonsoft.Json;
using Rest.Net.Interfaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoBot.API
{
    public class HistoryApi: IHistory
    {
        private readonly IRestClient _restClient;
        
        public HistoryApi(IRestClient restClient)
        {
            _restClient = restClient;
        }

        public async Task CloseOrder(IOrder order)
        {
            await SendOrder(order);
        }

        public async Task<IOrder> Last()
        {
            var result = await _restClient.GetAsync<dynamic>("/api/order/last", false);
           
            if (result.IsError)
                Console.WriteLine("HistoryApi | 'Last' Server error {0}", result.Data);

            var order = result.Data;

            return new Order()
            {
                Signal = order.signalType,
                Price = order.price,
                MarginLevel = order.marginLevel,
                Quantity = order.quantity,
                Total = order.total,
                Balance = order.balance,
                Time = order.time,
            };
        }

        public async Task OpenOrder(IOrder order)
        {
            await SendOrder(order);

           
        }

        private async Task SendOrder(IOrder order)
        {
            using (var client = new HttpClient())
            {
                var command = new
                {
                    signalID = "Wiliams",
                    signalType = order.Signal,
                    price = order.Price,
                    marginLevel = order.MarginLevel,
                    quantity = order.Quantity,
                    total = order.Total,
                    balance = order.Balance,
                    time = order.Time,
                };


                Console.WriteLine("LoggerApi | {0}", JsonConvert.SerializeObject(command));

//#if (DEBUG)
//                return;
//#endif

                var result = await _restClient.PostAsync<int>("/api/order", command, false);

                if (result.IsError)
                {
                    Console.WriteLine("HistoryApi | Server error {0}", result.Data);
                }
            }
        }
    }
}
