﻿using CryptoBot.Core;
using System;

namespace CryptoBot.API
{
    public static class MsgFactory
    {
        public static string Started() => "<b>Bot Started</b>";
        public static string Stoped() => "<b>Bot Stoped</b>";

        public static string Signal(Signal signal)
        {
            switch (signal)
            {
                case Core.Signal.Long:
                    return "<b>Long signal \uA71B</b>";
                case Core.Signal.Close:
                    return $"<b>Close signal</b>";
                case Core.Signal.Short:
                    return $"<b>Short signal \u2193</b>";
                default:
                    return string.Empty;
            }
        }
        public static string OrderOpened() => $"<b>Order alery openeed</b>";
        public static string OrderClosed() => $"<b>Order alery closed</b>";


        public static string NewCandle(ICandle candle) => $"<b>New Bar</b>\n{candle}";

        public static string PlaceOrderError(string error) => $"<b>Placeorder error</b>\n{error}";
        public static string RepayError(string error) => $"<b>Repay error</b>\n{error}";
        public static string BorrowError(string error) => $"<b>Borrow error</b>\n{error}";

        public static string PlaceOrderCompleated(IOrder order)
        {
            var side = Enum.GetName(typeof(Signal), order.Signal);

            return $"<b>Order {side} placed</b> Price: {order.Price:C} Balance: {order.Balance:C}";
        }

        public static string OrderNotFoundError(Guid id, string error) => $"<b> Order - {id} not found</b>\n{error}";

        public static string CriticalError(Exception exception) => $"<b>Critical error</b>\n{exception}";
    }
}
