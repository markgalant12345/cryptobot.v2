﻿using Binance.Net;
using CryptoBot.Core;

namespace CryptoBot.Helpers
{
    public class BrokerHelper
    {
        public static decimal NormalizQ(ISymbol symbol, decimal originalQ)
        {
            return BinanceHelpers.ClampQuantity(symbol.QuantityMin, symbol.QuantityMax, symbol.QuantityStep, originalQ);
        }

    }
}
