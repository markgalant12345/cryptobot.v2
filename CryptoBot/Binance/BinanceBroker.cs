﻿//using Binance.Net;
//using Binance.Net.Interfaces;
//using Binance.Net.Objects;
//using CryptoBot.API;
//using CryptoBot.Core;
//using CryptoBot.Helpers;
//using CryptoBot.Objects;
//using CryptoExchange.Net.Objects;
//using Microsoft.Extensions.Logging;
//using System;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;

//namespace CryptoBot.Binance
//{
//    public class BinanceBroker : IBroker
//    {
//        private readonly LoggerApi _log;
//        private readonly IBinanceClient _exchange;

//        public IHistory History { get; private set; }

//        public BinanceBroker(IBinanceClient exchange, IHistory history, LoggerApi log)
//        {
//            _exchange = exchange;
//            History = history;
//            _log = log;
//        }

//        public async Task ExecudeSignal(ISignal signal, ISource source, CancellationToken token = default(CancellationToken))
//        {
//            if (signal.Type == SignalType.Long)
//            {
//                await Long(signal, source, token);
//            }

//            if (signal.Type == SignalType.CloseLong)
//            {
//                await CloseLong(signal, source, token);
//            }
//        }

//        public async Task Long(ISignal signal, ISource source, CancellationToken token)
//        {
//            var symbol = source.Symbol;

//            var balance = await GetBalanceAsync(symbol.Market);
//            var price = await GetPriceAsync(symbol.Name);
//            var quntity = balance / price;
//            var normalizQ = BrokerHelper.NormalizQ(symbol, quntity);

//            if (normalizQ <= symbol.QuantityMin)
//                return;

//            var placedOrder = await PlaceOrderAsync(OrderSide.Buy, symbol, normalizQ, token);

//            if (placedOrder != null)
//            {
//                var order = await GetOrderAsync(placedOrder, signal, symbol.Name, token);

//                await History.OpenOrder(order);

//                await _log.LogInformation(MsgFactory.PlaceOrderCompleated(order));
//            }
//        }

//        public async Task CloseLong(ISignal signal, ISource source, CancellationToken token = default(CancellationToken))
//        {
//            var symbol = source.Symbol;

//            var quntity = await GetBalanceAsync(symbol.Asset);

//            if (quntity <= symbol.QuantityMin)
//                return;

//            var placeOrder = await PlaceOrderAsync(OrderSide.Sell, symbol, quntity, token);

//            if (placeOrder != null)
//            {
//                var order = await GetOrderAsync(placeOrder, signal, symbol.Name, token);

//                await History.CloseOrder(order);

//                await _log.LogInformation(MsgFactory.PlaceOrderCompleated(order));
//            }
//        }

//        private async Task<BinancePlacedOrder> PlaceOrderAsync(OrderSide side, ISymbol symbol, decimal quantity, CancellationToken token)
//        {
//            var result = await _exchange.PlaceOrderAsync(
//                symbol: symbol.Name,
//                side: side,
//                type: OrderType.Market,
//                quantity: quantity,
//                orderResponseType: OrderResponseType.Full,
//                ct: token
//            );

//            if (!result.Success)
//            {
//                await _log.LogCritical(MsgFactory.PlaceOrderError(result.Error.Message));

//                return null;
//            }

//            return result.Data;
//        }

//        private async Task<Order> GetOrderAsync(BinancePlacedOrder placeOrder, ISignal signal, string symbol, CancellationToken token)
//        {
//            var balance = await GetBalanceAsync();

//            return new Order()
//            {
//                Signal = signal,
//                Price = placeOrder.Fills.Average(t => t.Price),
//                Quantity = placeOrder.CummulativeQuoteQuantity,
//                Total = placeOrder.ExecutedQuantity,
//                Time = placeOrder.TransactTime,
//                Balance = balance
//            };
//        }

//        protected async Task<BinanceOrder> GetBinanceOrderAsync(Guid id, string symbol, CancellationToken token)
//        {
//            WebCallResult<BinanceOrder> result;

//            do
//            {
//                await Task.Delay(1000);

//                result = await _exchange.GetOrderAsync(symbol, origClientOrderId: id.ToString(), ct: token);
//            } while (result.Data.Status == OrderStatus.Filled);

//            if (!result.Success)
//            {
//                await _log.LogCritical(MsgFactory.OrderNotFoundError(id, result.Error.Message));
//            }

//            return result.Data;
//        }


//        private async Task<decimal> GetPriceAsync(string asset)
//        {
//            var price = await _exchange.GetPriceAsync(asset);

//            return price.Data.Price;
//        }

//        private async Task<decimal> GetBalanceAsync(string asset)
//        {
//            var account = await _exchange.GetAccountInfoAsync();

//            var balance = account.Data.Balances.FirstOrDefault(t => t.Asset == asset);

//            return balance.Free;
//        }

//        private async Task<decimal> GetBalanceAsync()
//        {
//            var market = "USDT";
//            var getAccountResult = await _exchange.GetAccountInfoAsync();
//            var getPricesResult = await _exchange.GetAllBookPricesAsync();

//            var prices = getPricesResult.Data.ToDictionary(k => k.Symbol, t => t.AskPrice);
//            var balances = getAccountResult.Data.Balances.ToDictionary(k => k.Asset, t => t.Total);

//            var balance = balances[market];

//            foreach (var binanceBalance in balances)
//            {
//                var symbol = binanceBalance.Key + market;

//                if (prices.ContainsKey(symbol))
//                    balance += binanceBalance.Value * prices[symbol];
//            }

//            return balance;
//        }
//    }
//}
