﻿using Binance.Net.Interfaces;
using Binance.Net.Objects;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoBot.Binance
{
    public class BinanceSourceManager : ISourceManager
    {
        private readonly IBinanceClient _client;
        private readonly IBinanceSocketClient _socketClient;

        public BinanceSourceManager(IBinanceClient client, IBinanceSocketClient socketClient)
        {
            _client = client;
            _socketClient = socketClient;
        }

        public async Task<ISource> CreateSourceAsync(string symbolName, TimeFrame timeFrame)
        {
            var symbol = await GetSymbol(symbolName);
            var klines = await GetKlinesAsync(symbolName, timeFrame);
            var source = new Source(klines, symbol, timeFrame);

            await _socketClient.SubscribeToKlineUpdatesAsync(symbolName, (KlineInterval)timeFrame, kline =>
            {
           
                source.Update(Convert(kline.Data.ToKline()));

                if (kline.Data.Final)
                    source.Add(Convert(kline.Data.ToKline()));
                    
            });

            return source;
        }

        private async Task<IList<ICandle>> GetKlinesAsync(string symbol, TimeFrame timeFrame)
        {
            var klines = await _client.GetKlinesAsync(symbol, (KlineInterval)timeFrame, endTime: DateTime.Now);

            return klines.Data.OrderByDescending(t => t.OpenTime).Select(Convert).ToList();
        }

        private async Task<ISymbol> GetSymbol(string symbol)
        {
            var info = await _client.GetExchangeInfoAsync();
            var item = info.Data.Symbols.FirstOrDefault(t => t.Name == symbol);

            return new Symbol()
            {
                Name = symbol,
                Market = item.QuoteAsset,
                Asset = item.BaseAsset,
                QuantityMax = item.LotSizeFilter.MaxQuantity,
                QuantityMin = item.LotSizeFilter.MinQuantity,
                QuantityStep = item.LotSizeFilter.StepSize,
            };
        }

        private ICandle Convert(BinanceKline kline)
        {
            return new Candle()
            {
                OpenTime = kline.OpenTime,
                Open = kline.Open,
                High = kline.High,
                Low = kline.Low,
                Close = kline.Close,
                CloseTime = kline.CloseTime,
                Volume = kline.Volume,
            };
        }
    }
}
