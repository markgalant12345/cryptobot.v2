﻿using Binance.Net;
using Binance.Net.Interfaces;
using Binance.Net.Objects;
using CryptoBot.API;
using CryptoBot.Core;
using CryptoBot.Core.Objects;
using CryptoBot.Helpers;
using CryptoBot.Objects;
using CryptoExchange.Net.Objects;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoBot.Binance
{
    public class BinanceMarginBroker : IBroker
    {
        public const decimal PRICE_RISK = 0.98m;
        private readonly LoggerApi _apiLogger;
        private readonly ILogger<BinanceMarginBroker> _logger;
        private readonly IBinanceClient _exchange;
        private readonly BrokerSettings _brokerSettings;

        public IHistory History { get; private set; }

        public BinanceMarginBroker(IBinanceClient exchange, IHistory history, BrokerSettings brokerSettings, LoggerApi apiLogger, ILogger<BinanceMarginBroker> logger)
        {
            _exchange = exchange;
            History = history;
            _brokerSettings = brokerSettings;
            _apiLogger = apiLogger;
            _logger = logger;
        }


        public async Task ExecudeSignal(Signal signal, ISource source, CancellationToken token = default(CancellationToken))
        {
            var last = await History.Last();
            var isLongAleryOpen = last.Signal == Signal.Long;
            var isShortAleryOpen = last.Signal == Signal.Short;
            var isClosedAll = last.Signal == Signal.Close;


            if (isShortAleryOpen && (signal == Signal.Long || signal == Signal.Close))
            {
                await CloseShort(source, token);

                return;
            }

            if (isLongAleryOpen && (signal == Signal.Short || signal == Signal.Close))
            {
                await CloseLong(source, token);

                return;
            }

            if (isClosedAll && signal == Signal.Long)
            {
                 await Long(source, token);

                return;
            }

            if (isClosedAll && signal == Signal.Short)
            {
                await Short(source, token);

                return;
            }
        }

        public async Task Long(ISource source, CancellationToken token)
        {
            var symbol = source.Symbol;

            if (await IsBorowed(symbol.Market))
            {
                _logger.LogDebug(MsgFactory.OrderOpened());
                return;
            }

            var price = await GetPriceAsync(symbol.Name);
            var quantity = await GetMaxBorrowAmountAsync(symbol.Market) / price;

            if (quantity <= symbol.QuantityMin)
                return;

            var placedOrder = await PlaceOrderAsync(OrderSide.Buy, symbol, quantity, SideEffectType.MarginBuy, token);

            if (placedOrder != null)
            {
                var balance = await GetBalanceAsync(symbol.Market);

                var order = await GetOrderAsync(placedOrder, Signal.Long, token);

                await History.OpenOrder(order);

                await _apiLogger.LogInformation(MsgFactory.PlaceOrderCompleated(order));
            }
        }

        public async Task Short(ISource source, CancellationToken token = default(CancellationToken))
        {
            var symbol = source.Symbol;

            if (await IsBorowed(symbol.Asset))
            {
                _logger.LogDebug(MsgFactory.OrderOpened());
                return;
            }

            var quantity = await GetMaxBorrowAmountAsync(symbol.Asset) ;

            if (quantity <= symbol.QuantityMin)
                return;

            var placedOrder = await PlaceOrderAsync(OrderSide.Sell, symbol, quantity, SideEffectType.MarginBuy, token);

            if (placedOrder != null)
            {
                var order = await GetOrderAsync(placedOrder, Signal.Short, token);

                await History.OpenOrder(order);

                await _apiLogger.LogInformation(MsgFactory.PlaceOrderCompleated(order));
            }
        }

        public async Task CloseLong(ISource source, CancellationToken token = default(CancellationToken))
        {
            var symbol = source.Symbol;

            if (!await IsBorowed(symbol.Market))
            {
                _logger.LogDebug(MsgFactory.OrderClosed());
                return;
            }

            var quantity = await GetBalanceAsync(symbol.Asset);

            if (quantity <= symbol.QuantityMin)
                return;

            var placeOrder = await PlaceOrderAsync(OrderSide.Sell, symbol, quantity, SideEffectType.AutoRepay, token);

            if (placeOrder != null)
            {
                var order = await GetOrderAsync(placeOrder, Signal.Close, token);

                await History.CloseOrder(order);

                await _apiLogger.LogInformation(MsgFactory.PlaceOrderCompleated(order));
            }
        }

        public async Task CloseShort(ISource source, CancellationToken token = default(CancellationToken))
        {
            var symbol = source.Symbol;

            if (!await IsBorowed(symbol.Asset))
            {
                _logger.LogDebug(MsgFactory.OrderClosed());
                return;
            }

            var quantity = await GetNeedRepay(symbol.Asset);

            if (quantity <= symbol.QuantityMin)
                return;

            var placeOrder = await PlaceOrderAsync(OrderSide.Buy, symbol, quantity, SideEffectType.AutoRepay, token);

            if (placeOrder != null)
            {
                var order = await GetOrderAsync(placeOrder, Signal.Close, token);

                await History.CloseOrder(order);

                await _apiLogger.LogInformation(MsgFactory.PlaceOrderCompleated(order));
            }
        }

        #region Internal

        private async Task<BinancePlacedOrder> PlaceOrderAsync(OrderSide side, ISymbol symbol, decimal quantity, SideEffectType type, CancellationToken token)
        {
            var result = await _exchange.PlaceMarginOrderAsync(
                symbol: symbol.Name,
                side: side,
                type: OrderType.Market,
                quantity: BrokerHelper.NormalizQ(symbol, quantity),
                orderResponseType: OrderResponseType.Full,
                sideEffectType: type,
                ct: token
            );

            if (!result.Success)
            {
                await Task.Delay(10 * 1000);

                await _apiLogger.LogCritical(MsgFactory.PlaceOrderError(result.Error.Message));

                return null;
            }

            await Task.Delay(5 * 1000);


            return result.Data;
        }

        private Task<Order> GetOrderAsync(BinancePlacedOrder placeOrder, Signal signal, CancellationToken token)
        {
            var order = new Order()
            {
                Signal = signal,
                Price = placeOrder.Fills.Average(t => t.Price),
                MarginLevel = _brokerSettings.MarginLevel,
                Quantity = placeOrder.CummulativeQuoteQuantity,
                Total = placeOrder.ExecutedQuantity,
                Time = placeOrder.TransactTime,
                Balance = placeOrder.CummulativeQuoteQuantity
            };


            return Task.FromResult(order);
        }

        private async Task<decimal> GetPriceAsync(string asset)
        {
            var price = await _exchange.GetPriceAsync(asset);

            return price.Data.Price;
        }

        private async Task<decimal> GetMaxBorrowAmountAsync(string asset)
        {
            var price = await _exchange.GetMaxBorrowAmountAsync(asset);

            return price.Data;
        }

        private async Task<decimal> GetBalanceAsync(string asset)
        {
            var account = await _exchange.GetMarginAccountInfoAsync();

            var balance = account.Data.Balances.FirstOrDefault(t => t.Asset == asset);

            //if (balance.Borrowed > decimal.Zero)
            //    return 0;

            return balance.Free;
        }

        private async Task<bool> IsBorowed(string asset)
        {
            var account = await _exchange.GetMarginAccountInfoAsync();

            var balance = account.Data.Balances.FirstOrDefault(t => t.Asset == asset);

            return balance.Borrowed > 0.1m;
        }

        private async Task<decimal> GetNeedRepay(string asset)
        {
            var account = await _exchange.GetMarginAccountInfoAsync();

            var balance = account.Data.Balances.FirstOrDefault(t => t.Asset == asset);

            return balance.Borrowed;
        }

        private async Task<bool> GetNeedRepay(string asset, decimal amount)
        {
            _logger.LogTrace($"Try repay {amount}");

            var repay = await _exchange.RepayAsync(asset, amount);

            if (!repay.Success)
            {
                _logger.LogDebug(repay.Error.Message);
            }

            _logger.LogTrace($"Repayed successful");

            return true;
        }

        #endregion Internal
    }
}
