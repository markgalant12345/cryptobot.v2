﻿namespace CryptoBot.Objects
{
    public class BrokerSettings
    {
        public decimal MarginLevel { get; set; }
        public decimal Fee { get; set; }
    }
}
