﻿using CryptoBot.API;
using CryptoBot.Binance;
using CryptoBot.Core;
using CryptoBot.Strategies;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoBot
{
    public class Bot : IHostedService
    {
        private readonly IBroker _broker;
        private readonly ISourceManager _sourceManager;
        private readonly LoggerApi _apiLogger;
        private readonly ILogger<Bot> _logger;
        public bool InProcess;

        public IStrategy Strategy { get; set; }
        public ISource Source { get; set; }

        public Bot(ISourceManager sourceManager, IBroker broker, LoggerApi apiLogger, ILogger<Bot> logger)
        {
            _broker = broker;
            _sourceManager = sourceManager;
            _apiLogger = apiLogger;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Strategy = new WilliamsMarkusStrategy();

            await Task.Delay(10000);

            await _apiLogger.LogDebug(MsgFactory.Started());

            Source = await _sourceManager.CreateSourceAsync("ETHUSDT", TimeFrame.FourHour);
            Source.OnChange += Work;
            
            _logger.LogInformation("Started");
        }

        private async void Work(object source, ICandle candle)
        {
            if (InProcess)
                return;

            try
            {
                InProcess = true;

                await RunStrategy(default);
            }
            catch (Exception ex)
            {
                _logger.LogError(1, ex, "Work");

                await _apiLogger.LogCritical(ex.ToString());

                throw ex;
            }
            finally
            {
                InProcess = false;
            }
        }

        public async Task RunStrategy(CancellationToken cancellationToken)
        {
            var signal = Strategy.Process(Source);

            _logger.LogTrace($"RunStrategy - {signal} ");

            if (signal == Signal.None)
                return;

            await _broker.ExecudeSignal(signal, Source, cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stop");
            await _apiLogger.LogDebug(MsgFactory.Stoped());
        }
    }
}
