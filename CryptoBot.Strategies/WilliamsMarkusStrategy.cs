﻿using CryptoBot.Core;
using CryptoBot.Indicators;
using System;
using System.Linq;

namespace CryptoBot.Strategies
{
    public class WilliamsMarkusStrategy : IStrategy
    {
        private int _acSlowLenght;
        private int _acFastLenght;
        private int _rLenght;
        private decimal _lontRTake;
        private decimal _shortRTake;

        public WilliamsMarkusStrategy(int acSlowLenght = 40,int acFastLenght = 5, int rLenght = 14, decimal lontRTake = -57m, decimal shortRTake = -57m)
        {
            _acSlowLenght = acSlowLenght;
            _acFastLenght = acFastLenght;
            _rLenght = rLenght;
            _lontRTake = lontRTake;
            _shortRTake = shortRTake;
        }

        public Signal Process(ISource source)
        {
            var ac = AC.Get(source, _acFastLenght, _acSlowLenght);
            var ac_previus = AC.Get(source.Skip(1), _acFastLenght, _acSlowLenght);
            var ac_previus_previus = AC.Get(source.Skip(2), _acFastLenght, _acSlowLenght);
            var r = WilliamsR.Get(source, _rLenght);

            if (!ac.HasValue || !r.HasValue)
                return Signal.None;

            var ac_t_p = ac_previus > ac_previus_previus;
            var ac_t = ac > ac_previus;

            if (ac_t && r > _lontRTake)
                return Signal.Long;


            if (!ac_t && r < _shortRTake)
                return Signal.Short;

            if (ac_t_p != ac_t)
                return Signal.Close;


            return Signal.None;

        }
    }
}
