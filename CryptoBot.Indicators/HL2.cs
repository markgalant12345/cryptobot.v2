﻿using CryptoBot.Core;

namespace CryptoBot.Indicators
{
    public class HL2
    {
        public static decimal Get(ICandle candle)
        {
            return (candle.High + candle.Low) / 2.0m;
        }
    }
}
