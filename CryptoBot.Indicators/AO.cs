﻿using CryptoBot.Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CryptoBot.Indicators
{
    public class AO
    {
        public static decimal? Get(IEnumerable<ICandle> source, int lengthFast = 5 , int lengthSlow = 34)
        {
            var sma1Hl2 = Sma.Get(source.Select(HL2.Get), lengthFast);
            var sma2Hl2 = Sma.Get(source.Select(HL2.Get), lengthSlow);

            if (sma2Hl2 == null || sma2Hl2 == null)
                return null;

            return sma1Hl2 - sma2Hl2;
        }
       
    }
}
