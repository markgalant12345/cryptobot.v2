﻿using CryptoBot.Core;
using System.Collections.Generic;
using System.Linq;

namespace CryptoBot.Indicators
{
    public class AC
    {
        public static decimal? Get(IEnumerable<ICandle> source, int lengthFast = 5, int lengthSlow = 34)
        {
            var count = source.Count();
            var ao = AO.Get(source, lengthFast, lengthSlow);

            var list = new List<decimal>();
            for(var i = lengthFast -1 ; i >= 0; i--)
            {
                var slice = source.Skip(i).Take(lengthSlow);
                var oldAO = AO.Get(slice , lengthFast, lengthSlow);

                if (!oldAO.HasValue)
                    return null;

                list.Add(oldAO.Value);
            }

            return ao - Sma.Get(list, lengthFast);
        }
       
    }
}
