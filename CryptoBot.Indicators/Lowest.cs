﻿using CryptoBot.Core;
using System.Linq;

namespace CryptoBot.Indicators
{
    public class Lowest
    {
        public static decimal Get(ISource candle, int length)
        {
            var items = candle.Select(t => t.Low).Take(length);
            return items.DefaultIfEmpty(0).Min();
        }
    }
}
