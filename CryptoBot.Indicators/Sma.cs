﻿using CryptoBot.Core;
using System.Collections.Generic;
using System.Linq;

namespace CryptoBot.Indicators
{
    public class Sma
    {
        public enum SmaType
        {
            Close,
            Open,
            High,
            Low
        }

        public static decimal? Get(IEnumerable<ICandle> source, int length, SmaType type = SmaType.Close)
        {
            if (source.Count() < length)
            {
                return null;
            }

            return source.Take(length).Select(c => ConvertType(c, type)).Sum() / length;
        }

        public static decimal? Get(IEnumerable<decimal> items, int length)
        {
            if (items.Count() < length)
            {
                return null;
            }

            return items.Take(length).Sum() / length;
        }

        private static decimal ConvertType(ICandle candle, SmaType type)
        {
            switch (type)
            {
                case SmaType.High:
                    return candle.High;
                case SmaType.Low:
                    return candle.Low;
                case SmaType.Open:
                    return candle.Open;
                default:
                    return candle.Close;
            }
        }
    }
}
