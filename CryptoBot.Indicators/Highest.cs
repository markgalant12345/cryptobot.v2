﻿using CryptoBot.Core;
using System.Linq;

namespace CryptoBot.Indicators
{
    public class Highest
    {
        public static decimal Get(ISource candle, int length)
        {
            return candle.Select(t => t.High).Take(length).DefaultIfEmpty(0).Max();
        }
    }
}
