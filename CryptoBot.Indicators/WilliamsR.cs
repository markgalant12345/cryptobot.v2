﻿using CryptoBot.Core;

namespace CryptoBot.Indicators
{
    public class WilliamsR
    {
        public static decimal? Get(ISource source, int length)
        {
            if (source.Candles.Count < length)
                return null;

            return 100.0m * (source.Candle.Close - Highest.Get(source, length)) / (Highest.Get(source, length) - Lowest.Get(source, length));
        }
    }
}
